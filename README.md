# Reto Tecnico: Api Cliente
API para visualizar y actualizar los datos de los clientes

## Descripcion de la Version requerida
1. Se obtiene los datos del cliente enviando el dato "codigo único"
2. Se puede actualizar la informacion del cliente
	a. Se valida el codigo único para la actualización de los datos 
	b. Se valida que el documento (tipo y numero) no pertenezcan a algun cliente registrado
3. Los datos se regitran en H2, cuanta con data inicalizada.


## Recursos 
Los recursos requeridos se encontraran ubicados en la raiz del repositorio en la carpeta "RECURSOS"
- data-postgres-cliente.sql
- reto-tecnico-base-2.0.0.jar
- DockerFileRetoJavaBase 
- IBK reto tecnico Cliente.postman_collection.json : pruebas en postman

## Instalación: Docker
Se requieren los siguientes archivos:
* reto-tecnico-base-2.0.0.jar
* DockerFileRetoJavaBase 

1. Generar la imagen mediante DockerFile:
```
	docker image build -t clientereqbase -f DockerFileRetoJavaBase .
```
2.Correr el contenedor
```
	docker run --rm -it -e "SPRING_PROFILES_ACTIVE=dev" -p 9876:9000 clientereqbase
	
```
Donde: 
clientereqbase es nombre de la imagen con versión por defecto, 
DockerFileRetoJavaBase nombre del archivo de construcción de imagen. 

Observacion:	
Los comandos se deben ejecutar en la carpeta donse se ubiquen los recursos necesarios.

## Data
|ID|DESCRIPCION_CORTA|FECHA_ACTUALIZACION|
|---|---|---|
|1|DNI|fecha modificacion|
|2|RUC|fecha modificacion|
|3|CE|fecha modificacion|
|4|PASAPORTE|fecha modificacion|


|CLIENTE_ID|APELLIDOS|CODIGO_UNICO|NOMBRES|NUMERO_DOCUMENTO|FECHA_ACTUALIZACION|TIPO_DOCUMENTO_ID|
|---|---|---|---|---|---|---|
|1|Cortado Hurtado|807542|Xavier|41234567|fecha modificacion|1|
|2|Telmet Lara|807544|Tania|91234567|fecha modificacion|2|
|3|Sánchez Gomez|119074|Javier|2.54637E+13|fecha modificacion|4|
|4|Rodriguez Flores|133991|Francisco|79466800|fecha modificacion|1|
|5|Gonzalez Sosa|159376|Carlos|72790674|fecha modificacion|1|
|6|Pérez Sánchez|170666|Antonio|83348023|fecha modificacion|1|
|7|Martínez González|172550|Miguel Ángel|48279048|fecha modificacion|1|
|8|García Diaz|218794|Juan|83114967|fecha modificacion|1|
|9|Gomez Romero|228917|Daniel|41090858|fecha modificacion|1|
|10|Ramírez Ramírez|266412|José|59015055|fecha modificacion|1|
|11|López Martínez|285133|José Manuel|1.24302E+13|fecha modificacion|4|
|12|González Torres|285924|Jesús|1.24867E+13|fecha modificacion|4|
|13|Martinez López|291923|Rafael|1.29152E+13|fecha modificacion|4|
|14|Sánchez Rodriguez|301970|José Antonio|65017015|fecha modificacion|1|
|15|Fernandez Fernandez|318574|Francisco|41820150|fecha modificacion|1|
|16|Rodríguez Gómez|377026|David|79466801566|fecha modificacion|2|
|17|Pérez Rodríguez|412661|Ángel|72790675760|fecha modificacion|2|
|18|García Martinez|467602|José Antonio|83348023972|fecha modificacion|2|
|19|Rodriguez Cruz|470670|Manuel|48279049343|fecha modificacion|2|
|20|Gomez Pérez|550916|Javier|83114968194|fecha modificacion|2|
|21|Gómez Sánchez|557169|José Luis|41090859350|fecha modificacion|2|
|22|Gonzalez García|576162|Francisco Javier|59015056300|fecha modificacion|2|
|23|Lopez García|633829|Miguel|65017016772|fecha modificacion|3|
|24|Romero Gonzalez|634256|Juan|80046254814|fecha modificacion|3|
|25|Diaz Rodriguez|658824|José|3.91224E+13|fecha modificacion|3|
|26|Pérez Pérez|701958|Pedro|7.69168E+12|fecha modificacion|3|
|27|Cruz Gonzalez|751496|Antonio|4.57419E+13|fecha modificacion|3|
|28|Torres Pérez|759539|José Luis|8.41478E+12|fecha modificacion|3|
|29|Hernández Hernández|783801|Alejandro|4.80494E+13|fecha modificacion|3|
|30|Flores Lopez|802137|David|4.93591E+13|fecha modificacion|3|
|31|Fernandez Gomez|805372|Manuel|4.95901E+13|fecha modificacion|3|
|32|Sosa Fernandez|863263|Daniel|3.14147E+13|fecha modificacion|3|
