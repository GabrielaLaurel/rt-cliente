package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.example.demo.entity.Cliente;
import com.example.demo.entity.TipoDocumento;
import com.example.demo.repository.ClienteRepository;
import com.example.demo.repository.TipoDocumentoRepository;

@Component
public class DataDev implements ApplicationListener<ContextRefreshedEvent>{

	@Autowired
	ClienteRepository clienteRepo;
	@Autowired
	private TipoDocumentoRepository tipoDocRepo;
	
	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		
		TipoDocumento dni =tipoDocRepo.save(new TipoDocumento("DNI"));
		TipoDocumento ruc =tipoDocRepo.save(new TipoDocumento("RUC"));
		TipoDocumento ce =tipoDocRepo.save(new TipoDocumento("CE"));
		TipoDocumento pasaporte =tipoDocRepo.save(new TipoDocumento("PASAPORTE"));
		
		clienteRepo.save(new Cliente("807542","Xavier","Cortado Hurtado",dni,"41234567"));
		clienteRepo.save(new Cliente("807544","Tania ","Telmet Lara",ruc,"91234567"));
		clienteRepo.save(new Cliente("119074","Javier","Sánchez Gomez",pasaporte,"25463723438849"));
		clienteRepo.save(new Cliente("133991","Francisco","Rodriguez Flores",dni,"79466800"));
		clienteRepo.save(new Cliente("159376","Carlos","Gonzalez Sosa",dni,"72790674"));
		clienteRepo.save(new Cliente("170666","Antonio","Pérez Sánchez",dni,"83348023"));
		clienteRepo.save(new Cliente("172550","Miguel Ángel","Martínez González",dni,"48279048"));
		clienteRepo.save(new Cliente("218794","Juan","García Diaz",dni,"83114967"));
		clienteRepo.save(new Cliente("228917","Daniel","Gomez Romero",dni,"41090858"));
		clienteRepo.save(new Cliente("266412","José","Ramírez Ramírez",dni,"59015055"));
		clienteRepo.save(new Cliente("285133","José Manuel","López Martínez",pasaporte,"12430206934600"));
		clienteRepo.save(new Cliente("285924","Jesús","González Torres",pasaporte,"12486715075733"));
		clienteRepo.save(new Cliente("291923","Rafael","Martinez López",pasaporte,"12915200751743"));
		clienteRepo.save(new Cliente("301970","José Antonio","Sánchez Rodriguez",dni,"65017015"));
		clienteRepo.save(new Cliente("318574","Francisco","Fernandez Fernandez",dni,"41820150"));
		clienteRepo.save(new Cliente("377026","David","Rodríguez Gómez",ruc,"79466801566"));
		clienteRepo.save(new Cliente("412661","Ángel","Pérez Rodríguez",ruc,"72790675760"));
		clienteRepo.save(new Cliente("467602","José Antonio","García Martinez",ruc,"83348023972"));
		clienteRepo.save(new Cliente("470670","Manuel","Rodriguez Cruz",ruc,"48279049343"));
		clienteRepo.save(new Cliente("550916","Javier","Gomez Pérez",ruc,"83114968194"));
		clienteRepo.save(new Cliente("557169","José Luis","Gómez Sánchez",ruc,"41090859350"));
		clienteRepo.save(new Cliente("576162","Francisco Javier","Gonzalez García",ruc,"59015056300"));
		clienteRepo.save(new Cliente("633829","Miguel","Lopez García",ce,"65017016772"));
		clienteRepo.save(new Cliente("634256","Juan","Romero Gonzalez",ce,"80046254814"));
		clienteRepo.save(new Cliente("658824","José","Diaz Rodriguez",ce,"39122402591520"));
		clienteRepo.save(new Cliente("701958","Pedro","Pérez Pérez",ce,"7691683791806"));
		clienteRepo.save(new Cliente("751496","Antonio","Cruz Gonzalez",ce,"45741855215207"));
		clienteRepo.save(new Cliente("759539","José Luis","Torres Pérez",ce,"8414784302588"));
		clienteRepo.save(new Cliente("783801","Alejandro","Hernández Hernández",ce,"48049357203093"));
		clienteRepo.save(new Cliente("802137","David","Flores Lopez",ce,"49359052801732"));
		clienteRepo.save(new Cliente("805372","Manuel","Fernandez Gomez",ce,"49590130184423"));
		clienteRepo.save(new Cliente("863263","Daniel","Sosa Fernandez",ce,"31414733672703"));
	}
}
