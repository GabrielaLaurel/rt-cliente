package com.example.demo.repository;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.entity.Cliente;

public interface ClienteRepository extends CrudRepository<Cliente, Long>{
	
	Cliente findByCodigoUnico(String codigoUnico);
	Boolean existsByCodigoUnico(String codigoUnico);
	Boolean existsByTipoDocumentoDescripcionAndNumeroDocumentoAndIdNot(String tipoDoc, String numDoc, long id);

}
