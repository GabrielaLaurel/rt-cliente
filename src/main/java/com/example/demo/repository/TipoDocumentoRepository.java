package com.example.demo.repository;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.entity.TipoDocumento;

public interface TipoDocumentoRepository extends CrudRepository<TipoDocumento, Long>{
	TipoDocumento findByDescripcionIgnoreCase(String tipoDoc);
	Boolean existsByDescripcionIgnoreCase(String tipoDoc);
}
