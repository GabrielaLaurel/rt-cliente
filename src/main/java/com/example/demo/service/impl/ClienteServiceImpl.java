package com.example.demo.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.ClienteRequest;
import com.example.demo.dto.ClienteResponse;
import com.example.demo.entity.Cliente;
import com.example.demo.exception.DatoNoValidadoException;
import com.example.demo.exception.DatosPreExistentesException;
import com.example.demo.exception.NoEncontradoException;
import com.example.demo.repository.ClienteRepository;
import com.example.demo.repository.TipoDocumentoRepository;
import com.example.demo.service.ClienteService;

@Service
public class ClienteServiceImpl implements ClienteService {
	@Autowired
	private ClienteRepository clienteRepository;
	@Autowired
	private TipoDocumentoRepository tipoDocumentoRepository;

	private Logger LOGGER = LoggerFactory.getLogger(ClienteServiceImpl.class);

	@Override
	public ClienteResponse obtenerClientePorCodigo(String codigoUnico) {
		LOGGER.info("***********INICIO obtenerClientePorCodigo***********");
		Cliente cliente = clienteRepository.findByCodigoUnico(codigoUnico);

		if (cliente != null) {
			ClienteResponse clienteResponse = new ClienteResponse();
			clienteResponse = new ClienteResponse(cliente.getNombres(), cliente.getApellidos(),
					cliente.getTipoDocumento().getDescripcion(), cliente.getNumeroDocumento());
			LOGGER.info("***********FIN obtenerClientePorCodigo***********");
			return clienteResponse;
		}
		throw new NoEncontradoException("NO SE ENCONTRO Cliente con codigo unico " + codigoUnico);
	}

	@Override
	public void actulaizarCliente(ClienteRequest clienteReq) {
		LOGGER.info("***********INICIO actulaizarCliente***********");
		if (!clienteRepository.existsByCodigoUnico(clienteReq.getCodigoUnico())) {
			throw new NoEncontradoException("NO SE ENCONTRO Cliente con codigo unico " + clienteReq.getCodigoUnico());
		}

		if (!tipoDocumentoRepository.existsByDescripcionIgnoreCase(clienteReq.getTipoDocumento())) {
			throw new NoEncontradoException("El tipo de documento " + clienteReq.getTipoDocumento() + ", No es valido");
		}
		Cliente cliente = clienteRepository.findByCodigoUnico(clienteReq.getCodigoUnico());

		if (clienteRepository.existsByTipoDocumentoDescripcionAndNumeroDocumentoAndIdNot(clienteReq.getTipoDocumento(),
				clienteReq.getNumeroDocumento(), cliente.getId())) {
			throw new DatosPreExistentesException();
		}

		if (clienteReq.getTipoDocumento().equalsIgnoreCase("RUC")) {
			if (clienteReq.getNumeroDocumento().length() != 11) {
				throw new DatoNoValidadoException("Longitud de RUC debe ser 11");
			}
		}
		if (clienteReq.getTipoDocumento().equalsIgnoreCase("DNI")) {
			if (clienteReq.getNumeroDocumento().length() != 8) {
				throw new DatoNoValidadoException("Longitud de DNI debe ser 8");
			}
		}
		
		cliente.setNombres(clienteReq.getNombres());
		cliente.setApellidos(clienteReq.getApellidos());
		cliente.setTipoDocumento(tipoDocumentoRepository.findByDescripcionIgnoreCase(clienteReq.getTipoDocumento()));
		cliente.setNumeroDocumento(clienteReq.getNumeroDocumento());
		clienteRepository.save(cliente);

		LOGGER.info("***********FIN actulaizarCliente***********");
	}

}
