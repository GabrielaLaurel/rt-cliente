package com.example.demo.service;

import com.example.demo.dto.ClienteRequest;
import com.example.demo.dto.ClienteResponse;
import com.example.demo.entity.Cliente;

public interface ClienteService {
	
	public ClienteResponse obtenerClientePorCodigo(String CodigoUnico);
	public void actulaizarCliente(ClienteRequest cliente) ;

}
