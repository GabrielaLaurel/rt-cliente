package com.example.demo.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class NoEncontradoException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	private String mensaje;

}
