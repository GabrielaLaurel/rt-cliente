package com.example.demo.exception;

import java.util.logging.Logger;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class AppExceptionHandler {
	
	private static final Logger LOGGER = Logger.getLogger("CLIENTE IMPL");
	
	@ResponseBody
	@ExceptionHandler(value = NoEncontradoException.class)
	public ResponseEntity<?> handleException(NoEncontradoException exception) {
		LOGGER.info("NO ENCONTRADO  "+ HttpStatus.NOT_FOUND+" , Mensaje: "+ exception.getMensaje());
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(exception.getMensaje());
	}

	@ResponseBody
	@ExceptionHandler(value = DatosPreExistentesException.class)
	public ResponseEntity<?> handleException(DatosPreExistentesException exception) {
		LOGGER.info("DATOS DUPLICADOS  "+ HttpStatus.CONFLICT+" , Mensaje: Tipo y número de documento ya se encuentran registrados");
		return ResponseEntity.status(HttpStatus.CONFLICT).body("Tipo y número de documento ya se encuentran registrados");
	}

	@ResponseBody
	@ExceptionHandler(value = DatoNoValidadoException.class)
	public ResponseEntity<?> handleException(DatoNoValidadoException exception) {
		LOGGER.info("DATOS NO VALIDOS  "+ HttpStatus.BAD_REQUEST+" , Mensaje: "+exception.getMensaje());
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMensaje());
	}

}
