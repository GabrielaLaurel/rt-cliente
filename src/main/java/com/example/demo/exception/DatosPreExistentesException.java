package com.example.demo.exception;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class DatosPreExistentesException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	private String mensaje;

}
