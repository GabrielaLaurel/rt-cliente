package com.example.demo.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ClienteRequest extends ClienteResponse{
	@NotNull(message = "Campo codigo unico no debe ser nulo")
	@NotEmpty(message = "Campo codigo unico no debe ser vacío")
	@NotBlank(message = "Campo codigo unico no debe ser vacío")
	private String codigoUnico;
	
}
