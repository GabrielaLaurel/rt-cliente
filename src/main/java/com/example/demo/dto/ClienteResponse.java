package com.example.demo.dto;


import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ClienteResponse {
	
	@NotNull(message = "Campo nombres no debe ser nulo")
	@NotEmpty(message = "Campo nombres no debe ser vacío")
	@NotBlank(message = "Campo nombres no debe ser vacío")
	private String nombres;
	
	@NotNull(message = "Campo apellidos no debe ser nulo")
	@NotEmpty(message = "Campo apellidos no debe ser vacío")
	@NotBlank(message = "Campo apellidos no debe ser vacío")
	private String apellidos;
	
	@NotNull(message = "Campo tipo documento no debe ser nulo")
	@NotEmpty(message = "Campo tipo documento no debe ser vacío")
	@NotBlank(message = "Campo tipo documento no debe ser vacío")
	private String tipoDocumento;
	
	@Size( max=15, message="Campo con logitud Maxima 15")
	@NotNull(message = "Campo numero documento no debe ser nulo")
	@NotEmpty(message = "Campo numero documento no debe ser vacío")
	@NotBlank(message = "Campo numero documento no debe ser vacío")
	private String numeroDocumento;

}
