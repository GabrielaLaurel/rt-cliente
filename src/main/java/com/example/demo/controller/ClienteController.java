package com.example.demo.controller;


import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.ClienteRequest;
import com.example.demo.service.ClienteService;

@RestController
@RequestMapping(value="/cliente",
		consumes = MediaType.APPLICATION_JSON_VALUE,
		produces = MediaType.APPLICATION_JSON_VALUE)
public class ClienteController {
	
	@Autowired
	ClienteService clienteService;
	
	private Logger LOGGER = LoggerFactory.getLogger(ClienteController.class);
	
	@GetMapping(value="/codigo/{codigoUnico}", consumes = MediaType.ALL_VALUE)
	public ResponseEntity<Object> getCliente(@PathVariable("codigoUnico") String codigoUnico){
		 LOGGER.info("***********METODO getCliente***********");
		return ResponseEntity.ok(clienteService.obtenerClientePorCodigo(codigoUnico));
	}
	
	@PutMapping
	public ResponseEntity<Object> actualizarCliente(@RequestBody @Valid ClienteRequest body ) {
		LOGGER.info("***********METODO actualizarCliente***********");
			clienteService.actulaizarCliente(body);
			return ResponseEntity.ok(HttpStatus.OK.value());

	}
}
